import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import broadcast from '../../components/broadcast';
import { RootState } from '../../reducers';
import {
  InstagramAction,
  createLiveStory,
  startLiveStory,
  stopLiveStory,
  saveLiveStory
} from '../../actions/instagramActions';
import { LiveCreateBroadcastResponseRootObject } from 'instagram-private-api/dist/responses';

const mapStateToProps = (state: RootState) => ({
    ig: state.instagram.ig
});

const mapDispatchToProps = (dispatch: Dispatch<InstagramAction>) => ({
    createLiveStory: (broadcast: LiveCreateBroadcastResponseRootObject) =>
    dispatch(createLiveStory(broadcast)),
    startLiveStory: () => dispatch(startLiveStory()),
    stopLiveStory: () => dispatch(stopLiveStory()),
    saveLiveStory: () => dispatch(saveLiveStory())
});

export const BroadcastContainer = connect(mapDispatchToProps)(Broadcast);
