import { connect } from 'react-redux';

import { RootState } from '../../../reducers';
import { Preview } from '../../../components/broadcast/preview';

const mapStateToProps = (state: RootState) => ({
  broadcast: state.instagram.broadcast
});

export const PreviewContainer = connect(mapStateToProps)(Preview);
