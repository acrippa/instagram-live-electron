import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Create } from '../../../components/broadcast/controls';
import { RootState } from '../../../reducers';
import { InstagramAction, createLiveStory } from '../../../actions/instagramActions';
import { LiveCreateBroadcastResponseRootObject } from 'instagram-private-api/dist/responses';

const mapStateToProps = (state: RootState) => ({
  ig: state.instagram.ig
});

const mapDispatchToProps = (dispatch: Dispatch<InstagramAction>) => ({
  createLiveStory: (broadcast: LiveCreateBroadcastResponseRootObject) =>
    dispatch(createLiveStory(broadcast))
});

export const CreateContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Create);
