import { connect } from 'react-redux';

import { Controls } from '../../../components/broadcast/controls';
import { RootState } from '../../../reducers';

const mapStateToProps = (state: RootState) => ({
  ig: state.instagram.ig,
  broadcastEnable: state.instagram.broadcastEnable,
  broadcasting: state.instagram.broadcasting,
  saving: state.instagram.saving
});

export const ControlsContainer = connect(mapStateToProps)(Controls);
