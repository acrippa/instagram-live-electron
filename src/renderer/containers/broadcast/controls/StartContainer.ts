import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Start } from '../../../components/broadcast/controls';
import { RootState } from '../../../reducers';
import { InstagramAction, startLiveStory } from '../../../actions/instagramActions';
import { LiveInfoResponseRootObject } from 'instagram-private-api/dist/responses';

const mapStateToProps = (state: RootState) => ({
  ig: state.instagram.ig,
  broadcast: state.instagram.broadcast
});

const mapDispatchToProps = (dispatch: Dispatch<InstagramAction>) => ({
  startLiveStory: (info: LiveInfoResponseRootObject) => dispatch(startLiveStory(info))
});

export const StartContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Start);
