import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Stop } from '../../../components/broadcast/controls';
import { RootState } from '../../../reducers';
import { InstagramAction, stopLiveStory } from '../../../actions/instagramActions';

const mapStateToProps = (state: RootState) => ({
  ig: state.instagram.ig,
  broadcast: state.instagram.broadcast
});

const mapDispatchToProps = (dispatch: Dispatch<InstagramAction>) => ({
  stopLiveStory: () => dispatch(stopLiveStory())
});

export const StopContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Stop);
