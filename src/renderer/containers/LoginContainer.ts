import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Login } from '../components/login';
import { RootState } from '../reducers';
import { InstagramAction, login, logout } from '../actions/instagramActions';

const mapStateToProps = (state: RootState) => ({
  ig: state.instagram.ig
});

const mapDispatchToProps = (dispatch: Dispatch<InstagramAction>) => ({
  login: (username: string) => dispatch(login(username))
});

export const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
