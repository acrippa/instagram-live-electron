import { connect } from 'react-redux';

import Instagram from '../components/Instagram';
import { RootState } from '../reducers';

const mapStateToProps = (state: RootState) => ({
  loggedIn: state.instagram.loggedIn
});

export const InstagramContainer = connect(mapStateToProps)(Instagram);
