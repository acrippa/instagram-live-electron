import * as React from 'react';
import { ControlsContainer, PreviewContainer } from '../../containers/broadcast';
import { Row, Col } from 'antd';
require('./Broadcast.scss');

const Broadcast: React.FunctionComponent<null> = () => (
  <div className="broadcast">
    <Row>
      <Col span={12}>
        <PreviewContainer />
        <ControlsContainer />
      </Col>
      <Col span={12} />
    </Row>
  </div>
);

export default Broadcast;
