import * as React from 'react';
import { Button } from 'antd';
import { IgApiClient } from 'instagram-private-api';
import { BroadcastInfo } from '../../../reducers/instagramReducer';

export interface StopProps {
  ig: IgApiClient;
  broadcast: BroadcastInfo;
  stopLiveStory: () => void;
}

export const Stop: React.FunctionComponent<StopProps> = ({ ig, broadcast, stopLiveStory }) => {
  const stop = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    try {
      (async () => {
        await ig.live.endBroadcast(broadcast.broadcast_id);
        stopLiveStory();
      })();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Button type="primary" htmlType="button" className="stop-live-story-button" onClick={stop}>
      Stop
    </Button>
  );
};
