import * as React from 'react';
import { IgApiClient } from 'instagram-private-api';
import {
  CreateContainer,
  StartContainer,
  StopContainer
} from '../../../containers/broadcast/controls';

export interface Props {
    ig: IgApiClient;
    broadcastEnable: boolean;
    broadcasting: boolean;
    saving: boolean;
}
const username = 'olivier.franz';
const password = 'DioInu93';

export const Controls: React.FunctionComponent<Props> = ({ ig }) => {
    return (
    <div className="controls">
      <CreateContainer />
      <StartContainer />
      <StopContainer />
    </div>
  );
};
