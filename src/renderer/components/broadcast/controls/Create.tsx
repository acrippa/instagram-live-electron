import * as React from 'react';
import { Button } from 'antd';
import { LiveCreateBroadcastResponseRootObject } from 'instagram-private-api/dist/responses';
import { IgApiClient } from 'instagram-private-api';

export interface CreateProps {
  ig: IgApiClient;
  createLiveStory: (broadcast: LiveCreateBroadcastResponseRootObject) => void;
}

export const Create: React.FunctionComponent<CreateProps> = ({ ig, createLiveStory }) => {
  const create = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    try {
      (async () => {
        const liveStory: LiveCreateBroadcastResponseRootObject = await ig.live.create({});
        createLiveStory(liveStory);
      })();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Button type="primary" htmlType="button" className="create-live-story-button" onClick={create}>
      Create
    </Button>
  );
};
