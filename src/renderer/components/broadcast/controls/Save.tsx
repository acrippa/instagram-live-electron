import * as React from 'react';
import { Button } from 'antd';
import { LiveCreateBroadcastResponseRootObject } from 'instagram-private-api/dist/responses';
import { IgApiClient } from 'instagram-private-api';

export interface Props {
  ig: IgApiClient;
  broadcast: LiveCreateBroadcastResponseRootObject;
  saveLiveStory: () => void;
}

const Save: React.FunctionComponent<Props> = ({ ig, broadcast, saveLiveStory }) => {
  const save = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    try {
      (async () => {
        // await ig.live.save(broadcast.broadcast_id)
        saveLiveStory();
      })();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Button type="primary" htmlType="button" className="save-live-story-button" onClick={save}>
      Save
    </Button>
  );
};

export default Save;
