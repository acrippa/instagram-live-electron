import * as React from 'react';
import { Button } from 'antd';
import { LiveInfoResponseRootObject } from 'instagram-private-api/dist/responses';
import { IgApiClient } from 'instagram-private-api';
import { BroadcastInfo } from '../../../reducers/instagramReducer';

export interface StartProps {
  ig: IgApiClient;
  broadcast: BroadcastInfo;
  startLiveStory: (info: LiveInfoResponseRootObject) => void;
}

export const Start: React.FunctionComponent<StartProps> = ({ ig, broadcast, startLiveStory }) => {
  const start = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
    try {
      (async () => {
        broadcast;
        await ig.live.start(broadcast.broadcast_id);
        const info: LiveInfoResponseRootObject = await ig.live.info(broadcast.broadcast_id);
        info.dash_playback_url;
        info.rtmp_playback_url;
        startLiveStory(info);
      })();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Button type="primary" htmlType="button" className="staet-live-story-button" onClick={start}>
      Start
    </Button>
  );
};
