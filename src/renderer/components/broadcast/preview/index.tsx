import * as React from 'react';
import { BroadcastInfo } from '../../../reducers/instagramReducer';

export interface Props {
  broadcast: BroadcastInfo;
}

export const Preview: React.FunctionComponent<Props> = ({ broadcast }) => {
  return (
    <div className="preview">
      {broadcast.broadcast_id}
      <br />
      {broadcast.upload_url}
      <br />
      {broadcast.rtmp_playback_url}
    </div>
  );
};
