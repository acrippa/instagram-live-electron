import * as React from 'react';
import * as Bluebird from 'bluebird';
import {
  IgApiClient,
  IgCheckpointError,
  IgLoginInvalidUserError,
  IgLoginBadPasswordError,
  IgResponseError,
  IgNetworkError
} from 'instagram-private-api';
import { Row, Col, Form, Input, Icon, Button } from 'antd';

require('./Login.scss');

export interface Props {
  ig: IgApiClient;
  login: (username: string) => void;
}

export const Login: React.FunctionComponent<Props> = ({ ig, login }) => {
  let username: string = '';
  let password: string = '';
  const setUsername = (e: React.ChangeEvent<HTMLInputElement>): void => {
    username = e.target.value;
  };
  const setPassword = (e: React.ChangeEvent<HTMLInputElement>): void => {
    password = e.target.value;
  };
  const onSubmit = (e: React.FormEvent<HTMLInputElement>): void => {
    e.preventDefault();
    ig.state.generateDevice(username);
    Bluebird.try(async () => {
      await ig.simulate.preLoginFlow();
      await ig.account.login(username, password);
      process.nextTick(async () => await ig.simulate.postLoginFlow());
      login(username);
    })
      .catch(IgLoginInvalidUserError, () => {})
      .catch(IgLoginBadPasswordError, () => {})
      .catch(IgResponseError, () => {})
      .catch(IgNetworkError, () => {})
      .catch(IgCheckpointError, async () => {
        console.log(ig.state.checkpoint); // Checkpoint info here
        await ig.challenge.auto(true); // Requesting sms-code or click "It was me" button
        console.log(ig.state.checkpoint); // Challenge info here
        const code = 'get challenge code from user';
        console.log(await ig.challenge.sendSecurityCode(code));
      });
  };
  return (
    <Row type="flex" justify="center" align="middle">
      <Col span={8}>
        <Form className="login-form" onSubmit={onSubmit}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
              onChange={setUsername}
            />
          </Form.Item>
          <Form.Item>
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
              onChange={setPassword}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
