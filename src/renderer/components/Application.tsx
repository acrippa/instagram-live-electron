import { hot } from 'react-hot-loader/root';
import * as React from 'react';
import 'antd/dist/antd.css';
import { InstagramContainer } from '../containers/InstagramContainer';

const Application = () => (
  <div>
    <InstagramContainer />
  </div>
);

export default hot(Application);
