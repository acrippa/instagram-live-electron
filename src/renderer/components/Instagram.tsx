import * as React from 'react';
import { LoginContainer, BroadcastContainer } from '../containers';
require('./Instagram.scss');

export interface Props {
    loggedIn: boolean
}
const Instagram: React.FunctionComponent<Props> = ({ loggedIn }) => (
    <div className="instagram">
        {loggedIn ? (<BroadcastContainer />) : (<LoginContainer />)}
    </div>
);

export default Instagram;
