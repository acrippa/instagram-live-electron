import { InstagramAction } from './instagramActions';

export type RootActions = InstagramAction[keyof InstagramAction];
