import { Action, ActionCreator } from 'redux';
import {
  LiveCreateBroadcastResponseRootObject,
  LiveInfoResponseRootObject
} from 'instagram-private-api/dist/responses';

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const CREATE_LIVE_STORY = 'CREATE_LIVE_STORY';
export const START_LIVE_STORY = 'START_LIVE_STORY';
export const STOP_LIVE_STORY = 'STOP_LIVE_STORY';
export const SAVE_LIVE_STORY = 'SAVE_LIVE_STORY';

export interface LoginAction extends Action {
    type: 'LOGIN';
    username: string;
}
export interface LogoutAction extends Action {
    type: 'LOGOUT';
}
export interface CreateLiveStoryAction extends Action {
    type: 'CREATE_LIVE_STORY';
    broadcast: LiveCreateBroadcastResponseRootObject;
}
export interface StartLiveStoryAction extends Action {
    type: 'START_LIVE_STORY';
    info: LiveInfoResponseRootObject;
}
export interface StopLiveStoryAction extends Action {
    type: 'STOP_LIVE_STORY';
}
export interface SaveLiveStoryAction extends Action {
    type: 'SAVE_LIVE_STORY';
}

export const login: ActionCreator<LoginAction> = (username: string) => ({
    type: LOGIN,
    username
});

export const logout: ActionCreator<LogoutAction> = () => ({
    type: LOGOUT
});

export const createLiveStory: ActionCreator<CreateLiveStoryAction> = (
  broadcast: LiveCreateBroadcastResponseRootObject
) => ({
    type: CREATE_LIVE_STORY,
    broadcast
});

export const startLiveStory: ActionCreator<StartLiveStoryAction> = (
  info: LiveInfoResponseRootObject
) => ({
    type: START_LIVE_STORY,
    info
});

export const stopLiveStory: ActionCreator<StopLiveStoryAction> = () => ({
    type: STOP_LIVE_STORY
});

export const saveLiveStory: ActionCreator<SaveLiveStoryAction> = () => ({
    type: SAVE_LIVE_STORY
});

export type InstagramAction =
  | LoginAction
  | LogoutAction
  | CreateLiveStoryAction
  | StartLiveStoryAction
  | StopLiveStoryAction
  | SaveLiveStoryAction;
