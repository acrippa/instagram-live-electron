import { Reducer } from 'redux';
import { IgApiClient } from 'instagram-private-api';
import {
  LOGIN,
  LOGOUT,
  CREATE_LIVE_STORY,
  START_LIVE_STORY,
  STOP_LIVE_STORY,
  SAVE_LIVE_STORY,
  InstagramAction
} from '../actions/instagramActions';

export interface BroadcastInfo {
  broadcast_id: string;
  upload_url: string;
  heartbeat_interval: number;
  dash_playback_url: string;
  rtmp_playback_url: string;
}

export interface InstagramState {
  broadcast: BroadcastInfo;
  broadcastEnable: boolean;
  broadcasting: boolean;
  loggedIn: boolean;
  ig: IgApiClient;
  saving: boolean;
}

const defaultState: InstagramState = {
  broadcast: {
    broadcast_id: '',
    upload_url: '',
    heartbeat_interval: 0,
    dash_playback_url: '',
    rtmp_playback_url: ''
  },
  broadcastEnable: false,
  broadcasting: false,
  ig: new IgApiClient(),
  loggedIn: false,
  saving: false
};

export const instagramReducer: Reducer<InstagramState> = (
  state = defaultState,
  action: InstagramAction
) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loggedIn: true
      };
    case LOGOUT:
      return {
        ...state,
        loggedIn: false
      };
    case CREATE_LIVE_STORY:
      return {
        ...state,
        broadcast: {
          broadcast_id: action.broadcast.broadcast_id,
          upload_url: action.broadcast.upload_url,
          heartbeat_interval: action.broadcast.heartbeat_interval,
          dash_playback_url: '',
          rtmp_playback_url: ''
        },
        broadcastEnable: true
      };
    case START_LIVE_STORY:
      return {
        ...state,
        broadcasting: true,
        broadcast: {
          ...state.broadcast,
          dash_playback_url: action.info.dash_playback_url,
          rtmp_playback_url: action.info.rtmp_playback_url
        }
      };
    case STOP_LIVE_STORY:
      return {
        ...state,
        broadcastEnable: false,
        broadcasting: false,
        saving: true
      };
    case SAVE_LIVE_STORY:
      return {
        ...state,
        saving: false
      };
    default:
      return state;
  }
};
