import { combineReducers } from 'redux';

import { InstagramState, instagramReducer } from './instagramReducer';

export interface RootState {
  instagram: InstagramState;
}

export const rootReducer = combineReducers<RootState | undefined>({
  instagram: instagramReducer
});
