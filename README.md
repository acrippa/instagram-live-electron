# Instagram Live Electron

### An Electron App to manage Instagram Live Stories 

Thanks to [dilame's](https://github.com/dilame/instagram-private-api) work

## About this project
This project was created using [Robinfr Boilerplate](https://github.com/Robinfr/electron-react-typescript). Still a work in progress.

## License
MIT © [A. Crippa](https://gitlab.com/ACrippa)
